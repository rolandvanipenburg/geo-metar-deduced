FROM perl:latest
COPY . /home
WORKDIR /home
RUN cpanm Module::Build
CMD perl Build.PL && ./Build --installdeps && ./Build test
